#/usr/bin/python
from twitter import *
from time import strftime
from textwrap import fill
from termcolor import colored
from email.utils import parsedate
from datetime import datetime
import time
import os
import pprint
import re

sleep_time = 1

def post_status():
	os.system("clear")
	print"This posts a tweet to the account activated with the app.\n"
	opt = raw_input("Enter the text you want to post:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	results = twitter.statuses.update(status = opt)
	print "updated status: %s" % opt
	raw_input("Press enter to continue.")
	menu()

def friends_fowllowees():
	os.system("clear")
	print"This lists a users followers.\n"
	opt = raw_input("Enter the friend you want to scan for followers:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	username = opt
	query = twitter.friends.ids(screen_name = username)
	print "found %d friends" % (len(query["ids"]))
	for n in range(0, len(query["ids"]), 100):
		ids = query["ids"][n:n+100]
		subquery = twitter.users.lookup(user_id = ids)
		for user in subquery:
			print " [%s] %s - %s" % ("*" if user["verified"] else " ", user["screen_name"], user["location"])
	raw_input()
	menu()

def friendship():
	os.system("clear")
	print"This checks the relation of friends.\n"
	opt = raw_input("Enter friend 1:\n")
	two = raw_input("Enter friend 2:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	source = opt
	target = two
	result = twitter.friendships.show(source_screen_name = source, target_screen_name = target)
	following = result["relationship"]["target"]["following"]
	follows   = result["relationship"]["target"]["followed_by"]

	print "%s following %s: %s" % (source, target, follows)
	print "%s following %s: %s" % (target, source, following)
	print "Press Enter to Continue"
	raw_input()
	menu()
    
def my_timeline():
	os.system("clear")
	print"This lists 50 last tweets on your timeline.\n"
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	statuses = twitter.statuses.home_timeline(count = 50)
	print statuses
	for status in statuses:
		print "(%s) @%s %s" % (status["created_at"], status["user"]["screen_name"], status["text"])
	raw_input("Press enter to continue.")
	menu()

def list_lists():
	os.system("clear")
	print"Lists the lists owned by each of a list of users. Use [username, username, username] for the list.\n"
	opt = raw_input("Enter the list of users:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	users = opt
	for user in users:
		print "@%s" % (user)
		result = twitter.lists.list(screen_name = user)
		for list in result:
			print " - %s (%d members)" % (list["name"], list["member_count"])
	raw_input("Press enter to continue.")
	menu()
	
def list_retweets():
	os.system("clear")
	print"Lists who has retweeted tweets from the given user.\n"
	opt = raw_input("Enter the user:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	user = opt
	results = twitter.statuses.user_timeline(screen_name = user)
	for status in results:
		print "@%s %s" % (user, status["text"])
		retweets = twitter.statuses.retweets._id(_id = status["id"])
		for retweet in retweets:
			print " - retweeted by %s" % (retweet["user"]["screen_name"])
	raw_input("Press enter to continue.")
	menu()
			
def search():
	os.system("clear")
	print"Basic search of tweets with a keyword.\n"
	opt = raw_input("Enter the keywords:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	query = twitter.search.tweets(q = opt)
	print "Search complete (%.3f seconds)" % (query["search_metadata"]["completed_in"])
	for result in query["statuses"]:
		print "(%s) @%s %s" % (result["created_at"], result["user"]["screen_name"], result["text"])
	raw_input("Press enter to continue.")
	menu()

def stream_url_extract():
	os.system("clear")
	print"Lists live results containing a URL link and the #OWS tag.\n"
	opt = raw_input("Enter the filter:\n")
	config = {}
	execfile("config.py", config)
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"])
	stream = TwitterStream(auth = auth, secure = True)
	tweet_iter = stream.statuses.filter(track = opt)
	for tweet in tweet_iter:
		print "(%s) @%s %s" % (tweet["created_at"], tweet["user"]["screen_name"], tweet["text"])
		for url in tweet["entities"]["urls"]:
			print " - found URL: %s" % url["expanded_url"]

def responder():
	os.system("clear")
	print"Responds to a tweet mentioning the name provided with the message provided.\n"
	opt = raw_input("Enter the mention:\n")
	mess = raw_input("Enter the message:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	stream = TwitterStream(domain = "userstream.twitter.com", auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]), secure = True)
	tweet_iter = stream.user()
	username = opt
	for tweet in tweet_iter:
		if "entities" not in tweet:
			continue
		mentions = tweet["entities"]["user_mentions"]
		mentioned_users = [ mention["screen_name"] for mention in mentions ]

		if username in mentioned_users:
			print mess + " , Sent to @%s " % tweet["user"]["screen_name"]
			status = mess + " , send automaticly to @%s from TheTwitNitter" % tweet["user"]["screen_name"]
			try:
				twitter.statuses.update(status = status)
			except Exception, e:
				print " - failed (maybe a duplicate?): %s" % e

			time.sleep(sleep_time)
			

def stream_search():
	os.system("clear")
	print"Searches live stream for keyword and lists info.\n"
	opt = raw_input("Enter the keywords:\n")
	config = {}
	execfile("config.py", config)
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"])
	stream = TwitterStream(auth = auth, secure = True)
	search_term = opt
	tweet_iter = stream.statuses.filter(track = search_term)
	pattern = re.compile("%s" % search_term, re.IGNORECASE)
	for tweet in tweet_iter:
		# turn the date string into a date object that python can handle
		timestamp = parsedate(tweet["created_at"])

		# now format this nicely into HH:MM:SS format
		timetext = strftime("%H:%M:%S", timestamp)

		# colour our tweet's time, user and text
		time_colored = colored(timetext, color = "white", attrs = [ "bold" ])
		user_colored = colored(tweet["user"]["screen_name"], "green")
		text_colored = tweet["text"]

		# replace each instance of our search terms with a highlighted version
		text_colored = pattern.sub(colored(search_term.upper(), "yellow"), text_colored)

		# add some indenting to each line and wrap the text nicely
		indent = " " * 11
		text_colored = fill(text_colored, 80, initial_indent = indent, subsequent_indent = indent)

		# now output our tweet
		print "(%s) @%s" % (time_colored, user_colored)
		print "%s" % (text_colored)

def tweet_rate():
	os.system("clear")
	print"Lists the tweet rate for keyword.\n"
	opt = raw_input("Enter the keywords:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	created_at_format = '%a %b %d %H:%M:%S +0000 %Y'
	terms = opt
	query = twitter.search.tweets(q = terms)
	results = query["statuses"]
	first_timestamp = datetime.strptime(results[0]["created_at"], created_at_format)
	last_timestamp = datetime.strptime(results[-1]["created_at"], created_at_format)
	total_dt = (first_timestamp - last_timestamp).total_seconds()
	mean_dt = total_dt / len(results)
	print "Average tweeting rate for '%s' between %s and %s: %.3fs" % (terms, results[-1]["created_at"], results[ 0]["created_at"], mean_dt)
	raw_input("Press enter to continue.")
	menu()


def user_search():
	os.system("clear")
	print"Lists users matching the keyword.\n"
	opt = raw_input("Enter the keywords:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	results = twitter.users.search(q = opt)
	for user in results:
		print "@%s (%s): %s" % (user["screen_name"], user["name"], user["location"])
	raw_input("Press enter to continue.")
	menu()
	
def user_timeline():
	os.system("clear")
	print"Displays a users timeline.\n"
	opt = raw_input("Enter the user:\n")
	config = {}
	execfile("config.py", config)
	twitter = Twitter(
	auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"]))
	user = opt
	results = twitter.statuses.user_timeline(screen_name = user)
	for status in results:
		print "(%s) %s" % (status["created_at"], status["text"].encode("ascii", "ignore"))
	raw_input("Press enter to continue.")
	menu()

def menu():
	os.system("clear")
	print"  ________       ______         _ __  _   ___ __  __           "
	print" /_  __/ /_  ___/_  __/      __(_) /_/ | / (_) /_/ /____  _____"
	print"  / / / __ \/ _ \/ / | | /| / / / __/  |/ / / __/ __/ _ \/ ___/"
	print" / / / / / /  __/ /  | |/ |/ / / /_/ /|  / / /_/ /_/  __/ /    "
	print"/_/ /_/ /_/\___/_/   |__/|__/_/\__/_/ |_/_/\__/\__/\___/_/     "
	print" "
	print"A python program using twitter api created by Damion Watson."
	print" "
	print" "
	print"1.)  Post a Status"
	print"2.)  Search Keywords"
	print"3.)  Search Users"
	print"4.)  Use Responder"
	print"5.)  Veiw Timeline"
	print"6.)  Show User Timeline"
	print"7.)  Tweet Rate"
	print"8.)  Stream Search"
	print"9.)  Url Extract"
	print"10.) Show Retweets"
	print"11.) List Lists"
	print"12.) Show Friendship"
	print"13.) Show Followees"
	opt = raw_input()
	if opt == "1":
		post_status()
	elif opt == "2":
		search()
	elif opt == "3":
		user_search()
	elif opt == "4":
		responder()
	elif opt == "5":
		my_timeline()
	elif opt == "6":
		user_timeline()
	elif opt == "7":
		tweet_rate()
	elif opt == "8":
		stream_search()
	elif opt == "9":
		stream_url_extract()
	elif opt == "10":
		list_retweets()
	elif opt == "11":
		list_lists()
	elif opt == "12":
		friendship()
	elif opt == "13":
		friends_fowllowees()
	else:
		print"Enter a valid option"
		menu()
	


def main():
	os.system("clear")
	print"  ________       ______         _ __  _   ___ __  __           "
	print" /_  __/ /_  ___/_  __/      __(_) /_/ | / (_) /_/ /____  _____"
	print"  / / / __ \/ _ \/ / | | /| / / / __/  |/ / / __/ __/ _ \/ ___/"
	print" / / / / / /  __/ /  | |/ |/ / / /_/ /|  / / /_/ /_/  __/ /    "
	print"/_/ /_/ /_/\___/_/   |__/|__/_/\__/_/ |_/_/\__/\__/\___/_/     "
	print" "
	print"A python program using twitter api created by Damion Watson."
	print" "
	print" "
	print"1.) Menu"
	print"2.) Change Account"
	print"3.) Exit"
	print"4.) About"
	opt = raw_input()
	if opt == "1":
		menu()
	elif opt == "2":
		acct()
	elif opt == "3":
		os.system("clear")
		exit(0)
	elif opt == "4":
		about()
	else:
		print"Please enter a number."
		main()

def start():
	os.system("clear")
    
	print"  ________       ______         _ __  _   ___ __  __           "
	print" /_  __/ /_  ___/_  __/      __(_) /_/ | / (_) /_/ /____  _____"
	print"  / / / __ \/ _ \/ / | | /| / / / __/  |/ / / __/ __/ _ \/ ___/"
	print" / / / / / /  __/ /  | |/ |/ / / /_/ /|  / / /_/ /_/  __/ /    "
	print"/_/ /_/ /_/\___/_/   |__/|__/_/\__/_/ |_/_/\__/\__/\___/_/     "
	print" "
	print"A python program using twitter api created by Damion Watson."
	print" "
	print" "
	
	
	opt = raw_input("Are You Already Athorized? Y/N ")
	if opt == "Y":
		main()
	elif opt == "N":
		print "1. Create a new Twitter application here: https://apps.twitter.com"
		print "When you have created the application, enter:"
		print "   your application name: ",
		app_name = raw_input()

		print "   your consumer key: ",
		consumer_key = raw_input()

		print "   your consumer secret: ",
		consumer_secret = raw_input()

		print "2. Now, authorize this application."
		print "You'll be forwarded to a web browser in two seconds."
		print

		time.sleep(2)

		access_key, access_secret = twitter.oauth_dance(app_name, consumer_key, consumer_secret)

		print "Done."
		print
		print "Now, replace the credentials in config.py with the below:"
		print
		print "consumer_key = '%s'" % consumer_key
		print "consumer_secret = '%s'" % consumer_secret
		print "access_key = '%s'" % access_key
		print "access_secret = '%s'" % access_secret
		print "pres enter to continue"
		raw_input()
		main()
	else:
		print "Enter Y or N please."
		start()

start()
